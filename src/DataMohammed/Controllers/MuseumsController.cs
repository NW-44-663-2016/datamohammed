using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataMohammed.Models;

namespace DataMohammed.Controllers
{
    public class MuseumsController : Controller
    {
        private AppDbContext _context;

        public MuseumsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Museums
        public IActionResult Index()
        {
            var appDbContext = _context.Museums.Include(m => m.Location);
            return View(appDbContext.ToList());
        }

        // GET: Museums/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Museum museum = _context.Museums.Single(m => m.MuseumID == id);
            if (museum == null)
            {
                return HttpNotFound();
            }

            return View(museum);
        }

        // GET: Museums/Create
        public IActionResult Create()
        {
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: Museums/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Museum museum)
        {
            if (ModelState.IsValid)
            {
                _context.Museums.Add(museum);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", museum.LocationID);
            return View(museum);
        }

        // GET: Museums/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Museum museum = _context.Museums.Single(m => m.MuseumID == id);
            if (museum == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", museum.LocationID);
            return View(museum);
        }

        // POST: Museums/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Museum museum)
        {
            if (ModelState.IsValid)
            {
                _context.Update(museum);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", museum.LocationID);
            return View(museum);
        }

        // GET: Museums/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Museum museum = _context.Museums.Single(m => m.MuseumID == id);
            if (museum == null)
            {
                return HttpNotFound();
            }

            return View(museum);
        }

        // POST: Museums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Museum museum = _context.Museums.Single(m => m.MuseumID == id);
            _context.Museums.Remove(museum);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
