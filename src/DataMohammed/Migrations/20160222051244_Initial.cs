using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace DataMohammed.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    LocationID = table.Column<int>(nullable: false),
                    Country = table.Column<string>(nullable: true),
                    County = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    MuseumMuseumID = table.Column<int>(nullable: true),
                    Place = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    StateAbbreviation = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.LocationID);
                });
            migrationBuilder.CreateTable(
                name: "Museum",
                columns: table => new
                {
                    MuseumID = table.Column<int>(nullable: false),
                    AreaInSqft = table.Column<double>(nullable: false),
                    Art = table.Column<string>(nullable: true),
                    ArtCollectionType = table.Column<string>(nullable: true),
                    LocationID = table.Column<int>(nullable: true),
                    MuseumName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Museum", x => x.MuseumID);
                    table.ForeignKey(
                        name: "FK_Museum_Location_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Location",
                        principalColumn: "LocationID",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_Location_Museum_MuseumMuseumID",
                table: "Location",
                column: "MuseumMuseumID",
                principalTable: "Museum",
                principalColumn: "MuseumID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Museum_Location_LocationID", table: "Museum");
            migrationBuilder.DropTable("Location");
            migrationBuilder.DropTable("Museum");
        }
    }
}
