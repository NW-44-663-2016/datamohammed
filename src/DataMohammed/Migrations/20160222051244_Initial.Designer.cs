using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataMohammed.Models;

namespace DataMohammed.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160222051244_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataMohammed.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<int?>("MuseumMuseumID");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataMohammed.Models.Museum", b =>
                {
                    b.Property<int>("MuseumID");

                    b.Property<double>("AreaInSqft");

                    b.Property<string>("Art");

                    b.Property<string>("ArtCollectionType");

                    b.Property<int?>("LocationID");

                    b.Property<string>("MuseumName")
                        .IsRequired();

                    b.HasKey("MuseumID");
                });

            modelBuilder.Entity("DataMohammed.Models.Location", b =>
                {
                    b.HasOne("DataMohammed.Models.Museum")
                        .WithMany()
                        .HasForeignKey("MuseumMuseumID");
                });

            modelBuilder.Entity("DataMohammed.Models.Museum", b =>
                {
                    b.HasOne("DataMohammed.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
