﻿using DataMohammed.Models;
using Microsoft.Data.Entity;


namespace DataMohammed.Models
{

    public class AppDbContext : DbContext
    {

        public DbSet<Location> Locations { get; set; }
        public DbSet<Museum> Museums { get; set; }
    }
}
