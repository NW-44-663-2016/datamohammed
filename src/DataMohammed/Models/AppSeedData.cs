﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using DataMohammed.Models;
using System.Collections.Generic;
using System.IO;
using System.Data.SqlClient;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Data.Entity;


namespace DataMohammed.Models
{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.Museums.RemoveRange(context.Museums);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            
            SeedLocationsFromCsv(relPath, context);
            SeedMuseumsFromCsv(relPath, context);

            context.SaveChanges();



            //if (context.Locations.Any())
            //{

            //    return;   // DB already seeded
            //}
            ////Adding content for museums
            //if(context.Museums.Any())
            //{
            //    return; 
            //}

            //var locations = new List<Location>
            //{
            //    new Location { Latitude = 39.7634706, Longitude = -78.115095, Place = "Baltimore", State = "Maryland", Country = "USA" },
            //    new Location() { Latitude = 48.8606146, Longitude = 2.3354553, Place = "Paris", Country = "France" },
            //    new Location() { Latitude = 39.1346674, Longitude = -77.6365342, Place = "Baltimore", State = "Maryland", Country = "USA" },
            //    new Location() { Latitude = 38.8925139, Longitude = -77.0320221, Place = "WashingtonDC", Country = "USA" },
            //    new Location() { Latitude = 51.5176216, Longitude = -0.0989669, Place = "London", Country = "UK" },
            //    new Location() { Latitude = 30.0476568, Longitude = 31.2314263, Place = "Ismailia", Country = "Egypt" },
            //    new Location() { Latitude = 37.9684541, Longitude = 23.726334, Place = "Athina", Country = "Greece" }

            //};


            //context.Museums.AddRange(
            //    new Museum() { MuseumName= "Smithsonian Institution", AreaInSqft= 500000, Art= "First Ladies' Gown", ArtCollectionType = "artworks, artifacts and specimens", LocationID = l1.LocationID},
            //    new Museum() { MuseumName = "The Louvre ", AreaInSqft = 606000, Art = "The Mona Lisa", ArtCollectionType = "Islamic art, paintings and sculpture",LocationID = l2.LocationID},
            //    new Museum() { MuseumName = "National Gallery of Art", AreaInSqft = 400000, Art = "The Skater", ArtCollectionType = "paintings and sculpture",LocationID= l3.LocationID},
            //    new Museum() { MuseumName = "National Museum of Natural History", AreaInSqft = 350000, Art = "Gachalá Emerald", ArtCollectionType= " anthropology, botany, entomology, invertebrate zoology, mineral sciences", LocationID = l4.Entity.LocationID },
            //    new Museum() { MuseumName = "London Museum", AreaInSqft = 300000, Art = "Billingsgateh", ArtCollectionType = " Archaeology",LocationID = l5.Entity.LocationID},
            //    new Museum() { MuseumName = "Egyptian Museum", AreaInSqft = 280000, Art = "Gold Mask of Tutankhamun", ArtCollectionType = "Pharaonic antiquities", LocationID = l6.Entity.LocationID },
            //    new Museum() { MuseumName = "The Acropolis Museum", AreaInSqft = 3400000,Art = "The frieze of the Parthenon", ArtCollectionType = "Greek antiques",LocationID = l7.Entity.LocationID }
            //    );
            //context.SaveChanges();
        }

        private static void SeedMuseumsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "museum.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Museum.ReadAllFromCSV(source);
            
            List<Museum> lst = Museum.ReadAllFromCSV(source);
            context.Museums.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}
